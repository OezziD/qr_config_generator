- [qr_config_generator](#qr_config_generator)
  - [how to install the project](#how-to-install-the-project)


# qr_config_generator

Configure the email and other settings for the hwg glass by creating QR Codes

## how to install the project

clone the project and go into the directory qr_config_generator and execute following command

`flutter create .`

With `flutter run` you can build and run your application afterwards